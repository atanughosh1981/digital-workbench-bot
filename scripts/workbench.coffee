# Description:
#   Runs a command on hubot
#   TOTAL VIOLATION of any and all security! 
#
# Commands:
#   hubot bootstrap <command> - runs a command on hubot host

module.exports = (robot) ->
  robot.respond /project bootstrap (.*):(.*):(.*):(.*)$/i, (msg) ->
    console.log(msg)
    @exec = require('child_process').exec
    project = msg.match[1]
    gateway = msg.match[2]
    service = msg.match[3]
    jdlFile = msg.match[4]
    msg.send "Start Project Base Bootstrap... Setting up required files and librarues for Project, #{project}"
    basebootcmd    = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapBase -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    gatewaybootcmd = "python /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapGateway -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    servicebootcmd = "python /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapService -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    importJdl      = "python /Users/atanughosh/Documents/starter/kickstart.py -o ImportJdl -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service} -j /Users/atanughosh/Documents/starter/bank-jdl-jhipster.jh"

    msg.send "Start Project Base Bootstrap... Setting up required files and libraries for Project, #{project}"
    # msg.send "About to run Command: #{basebootcmd}" 
    msg.send "This operation may need several minutes to complete. You will be notified by me once the operation is completed..."
    @exec basebootcmd, (error, stdout, stderr) -> 
      if error
        msg.send error
        msg.send stderr
      else
        msg.send stdout
        msg.send "Project, #{project} bootstrap completed..."


  robot.respond /gateway bootstrap (.*):(.*):(.*):(.*)$/i, (msg) ->
    console.log(msg)
    @exec = require('child_process').exec
    project = msg.match[1]
    gateway = msg.match[2]
    service = msg.match[3]
    jdlFile = msg.match[4]
    msg.send "Start Gateway Bootstrap... Setting up required files and librarues for Project, #{gateway}"
    basebootcmd    = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapBase -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    gatewaybootcmd = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapGateway -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    servicebootcmd = "python /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapService -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    importJdl      = "python /Users/atanughosh/Documents/starter/kickstart.py -o ImportJdl -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service} -j /Users/atanughosh/Documents/starter/bank-jdl-jhipster.jh"

    msg.send "Start Gateway Application Bootstrap... Setting up required files and libraries for Gateway, #{gateway}"
    # msg.send "About to run Command: #{gatewaybootcmd}"
    msg.send "This operation may need several minutes to complete. You will be notified by me once the operation is completed..."
    @exec gatewaybootcmd, (error, stdout, stderr) ->
      if error
        msg.send error
        msg.send stderr
      else
        msg.send stdout
        msg.send "Gateway, #{gateway} bootstrap completed..."


  robot.respond /service bootstrap (.*):(.*):(.*):(.*)$/i, (msg) ->
    console.log(msg)
    @exec = require('child_process').exec
    project = msg.match[1]
    gateway = msg.match[2]
    service = msg.match[3]
    jdlFile = msg.match[4]
    msg.send "Start Microservice Application Bootstrap... Setting up required files and librarues for Service, #{service}"
    basebootcmd    = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapBase -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    gatewaybootcmd = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapGateway -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    servicebootcmd = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapService -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    importJdl      = "python /Users/atanughosh/Documents/starter/kickstart.py -o ImportJdl -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service} -j /Users/atanughosh/Documents/starter/bank-jdl-jhipster.jh"

    # msg.send "About to run Command: #{servicebootcmd}"
    msg.send "This operation may need several minutes to complete. You will be notified by me once the operation is completed..."
    @exec servicebootcmd, (error, stdout, stderr) ->
      if error
        msg.send error
        msg.send stderr
      else
        msg.send stdout
        msg.send "Service, #{service} bootstrap completed..."

  robot.respond /entity bootstrap (.*):(.*):(.*):(.*)$/i, (msg) ->
    console.log(msg)
    @exec = require('child_process').spawn
    project = msg.match[1]
    gateway = msg.match[2]
    service = msg.match[3]
    jdlFile = msg.match[4]
    msg.send "Start Entity creation... Auto generating the SpringBoot Java Sources and UI Component for the entities from #{jdlFile}"
    basebootcmd    = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapBase -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    gatewaybootcmd = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapGateway -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    servicebootcmd = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o BootstrapService -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service}"
    importJdl      = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o ImportJdl -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service} -j #{jdlFile}"

    # msg.send "About to run Command: #{importJdl}"
    msg.send "This operation may need several minutes to complete. You will be notified by me once the operation is completed..."
    commandRun = 
    @exec importJdl, (error, stdout, stderr) ->
      if error
        msg.send error
        msg.send stderr
      else
        msg.send stdout
        msg.send "Entity bootstrap completed..."

  robot.respond /entity spawn (.*):(.*):(.*):(.*)$/i, (msg) ->
    console.log(msg)
    @exec = require('child_process').spawn
    project = msg.match[1]
    gateway = msg.match[2]
    service = msg.match[3]
    jdlFile = msg.match[4]
    msg.send "Start Entity creation... Auto generating the SpringBoot Java Sources and UI Component for the entities from #{jdlFile}"
    importJdl      = "python -u /Users/atanughosh/Documents/starter/kickstart.py -o ImportJdl -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service} -j #{jdlFile}"
    importJdlOption      = "/Users/atanughosh/Documents/starter/kickstart.py -o ImportJdl -b /Users/atanughosh/Documents/demoauto/#{project}/ -g #{gateway} -s #{service} -j #{jdlFile}"
    msg.send (importJdlOption)

    msg.send "This operation may need several minutes to complete. You will be notified by me once the operation is completed..."
    commandSpawn = require('child_process').spawn
    entityCmd    = spawn('python', ['-u', importJdlOption])
    entityCmd.stdout.on 'data', (data) ->
        msg.send data
    
    entityCmd.stderr.on 'data', (data) ->
        msg.send data

    entityCmd.on 'exit', (code) ->
        msg.send "Process done with code #{code}"

